/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package MyPackage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author hahander
 */
public class DbConnect {
    public static Connection connect() {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:test.db");
        } catch ( Exception e ) {
            System.out.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return c;
    }
    
    public static void createHouse() {
        Connection c = connect();
        try {
            Statement stmt = c.createStatement();
            String sql = "CREATE TABLE IF NOT EXISTS house" +
                   "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                   "address TEXT NOT NULL," +
                   "jeu_id INT NOT NULL," +
                   "level_number INT NOT NULL," +
                   "wall_type_id INT NOT NULL," + 
                   "year_of_build CHAR(4) NOT NULL)";
            stmt.executeUpdate(sql);
            stmt.close();
            c.close();
        } catch (SQLException e) {
           System.err.println( e.getClass().getName() + ": createHouse " + e.getMessage() );  
        }
    }
    
    public static void createApartment() {
        Connection c = connect();
        try {
            Statement stmt = c.createStatement();
            String sql = "CREATE TABLE IF NOT EXISTS apartment"
                    + "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                    + "number INT NOT NULL,"
                    + "owner TEXT NOT NULL,"
                    + "status_id INT NOT NULL,"
                    + "rooms_count INT NOT NULL,"
                    + "solution_number TEXT NOT NULL,"
                    + "solution_date DATE NOT NULL,"
                    + "work_type_id INT NOT NULL,"
                    + "house_id INT NOT NULL)";
            stmt.executeUpdate(sql);
            stmt.close();
            c.close();
        } catch (SQLException e) {
            System.err.println( e.getClass().getName() + ": createApartment " + e.getMessage() );
        }
    }
    
    public static void createJeu() {
        Connection c = connect();
        try {
            Statement stmt = c.createStatement();
            String sql = "CREATE TABLE IF NOT EXISTS jeu"
                    + "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                    + "name TEXT NOT NULL)";
            stmt.executeUpdate(sql);
            stmt.close();
            c.close();
        } catch (SQLException e) {
            System.err.println( e.getClass().getName() + ": createJeu " + e.getMessage() );
        }
    }
    
    public static void createStatus() {
        Connection c = connect();
        try {
            Statement stmt = c.createStatement();
            String sql = "CREATE TABLE IF NOT EXISTS status"
                    + "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                    + "name TEXT NOT NULL)";
            stmt.executeUpdate(sql);
            stmt.close();
            c.close();
        } catch (SQLException e) {
            System.err.println( e.getClass().getName() + ": createStatus " + e.getMessage() );
        }
    }
    
    public static void createWallType() {
        Connection c = connect();
        try {
            Statement stmt = c.createStatement();
            String sql = "CREATE TABLE IF NOT EXISTS wall_type"
                    + "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                    + "name TEXT NOT NULL)";
            stmt.executeUpdate(sql);
            stmt.close();
            c.close();
        } catch (SQLException e) {
            System.err.println( e.getClass().getName() + ": createWallType " + e.getMessage() );
        }
    }
    
    public static void createWorkType() {
        Connection c = connect();
        try {
            Statement stmt = c.createStatement();
            String sql = "CREATE TABLE IF NOT EXISTS work_type"
                    + "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                    + "name TEXT NOT NULL)";
            stmt.executeUpdate(sql);
            stmt.close();
            c.close();
        } catch (SQLException e) {
            System.err.println( e.getClass().getName() + ": createWorkType " + e.getMessage() );
        }
    }
    
    public static void addNumber() {
        Connection c = connect();
        try {
            Statement stmt = c.createStatement();
            String sql = "ALTER TABLE apartment ADD number INT;";
            stmt.executeUpdate(sql);
            stmt.close();
            c.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }
    
    public static void dropAll() {
        Connection c = connect();
        String[] tables = {"house", "apartment", "jeu", "status", "work_type", "wall_type"};
        for (int i = 0; i < tables.length; i++) {
            try {
                Statement stmt = c.createStatement();
                String sql = "DROP TABLE " + tables[i];
                stmt.executeUpdate(sql);
                stmt.close();
            } catch (SQLException e) {
                System.err.println(e.getMessage());
            }
        }
        try {
            c.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }    
    }
}
