/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package MyPackage;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hahander
 */
public class House {
    private int id;
    private String address;
    private Jeu jeu;
    private int level_number;
    private WallType wall_type;
    private String year_of_build;
    
    public House(int id, String address, Jeu jeu, 
            int level_number, WallType wall_type, 
            String year_of_build) {
        this.id = id;
        this.address = address;
        this.jeu = jeu;
        this.level_number = level_number;
        this.wall_type = wall_type;
        this.year_of_build = year_of_build;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }
  

    /**
     * @return the jeu_name
     */
    public Jeu getJeu() {
        return jeu;
    }

    /**
     * @param jeu_name the jeu_name to set
     */
    public void setJeu(Jeu jeu) {
        this.jeu = jeu;
    }

    /**
     * @return the level_number
     */
    public int getLevel_number() {
        return level_number;
    }

    /**
     * @param level_number the level_number to set
     */
    public void setLevel_number(int level_number) {
        this.level_number = level_number;
    }

    /**
     * @return the wall_type_name
     */
    public WallType getWall_type() {
        return wall_type;
    }

    /**
     * @param wall_type_name the wall_type_name to set
     */
    public void setWall_type(WallType wall_type) {
        this.wall_type = wall_type;
    }

    /**
     * @return the year_of_build
     */
    public String getYear_of_build() {
        return year_of_build;
    }

    /**
     * @param year_of_build the year_of_build to set
     */
    public void setYear_of_build(String year_of_build) {
        this.year_of_build = year_of_build;
    }
       
    public static List<House> selectAll() {
        List<House> houses = new ArrayList<House>();
        Connection c = DbConnect.connect();
        try {
            Statement stmt = c.createStatement();
            String sql = "SELECT house.id, house.address, house.jeu_id, "
                    + "house.level_number, house.wall_type_id, house.year_of_build, "
                    + "jeu.id AS jeuId, jeu.name AS jeuName, "
                    + "wall_type.id AS wall_typeId, wall_type.name AS wall_typeName "
                    + "FROM house "
                    + "INNER JOIN jeu ON house.jeu_id = jeu.id "
                    + "INNER JOIN wall_type ON house.wall_type_id = wall_type.id";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()) {
                int id = rs.getInt("id");
                String address = rs.getString("address");
                int jeu_id = rs.getInt("jeuId");
                String jeu_name = rs.getString("jeuName");
                int level_number = rs.getInt("level_number");
                int wall_type_id = rs.getInt("wall_typeId");
                String wall_type_name = rs.getString("wall_typeName");
                String year_of_build = rs.getString("year_of_build");
                houses.add(new House(id, address, new Jeu(jeu_id, jeu_name), 
                        level_number, new WallType(wall_type_id, wall_type_name), 
                        year_of_build));
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return houses;
    }
    
    public static List<House> search(String search_param) {
        List<House> houses = new ArrayList<House>();
        Connection c = DbConnect.connect();
        try {
            Statement stmt = c.createStatement();
            String sql = "SELECT house.id, house.address, house.jeu_id, "
                        + "house.level_number, house.wall_type_id, house.year_of_build, "
                        + "jeu.id AS jeuId, jeu.name AS jeuName, "
                        + "wall_type.id AS wall_typeId, wall_type.name AS wall_typeName "
                        + "FROM house "
                        + "INNER JOIN jeu ON house.jeu_id = jeu.id "
                        + "INNER JOIN wall_type ON house.wall_type_id = wall_type.id "
                        + "WHERE house.address LIKE '%"+search_param+"%'";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()) {
                int id = rs.getInt("id");
                String address = rs.getString("address");
                int jeu_id = rs.getInt("jeuId");
                String jeu_name = rs.getString("jeuName");
                int level_number = rs.getInt("level_number");
                int wall_type_id = rs.getInt("wall_typeId");
                String wall_type_name = rs.getString("wall_typeName");
                String year_of_build = rs.getString("year_of_build");
                houses.add(new House(id, address, new Jeu(jeu_id, jeu_name), 
                        level_number, new WallType(wall_type_id, wall_type_name), 
                        year_of_build)); 
            }
            rs.close();
            stmt.close();
            c.close();
        }   catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return houses;
    }
    
    public static void insert(String address, String jeu_id, String level_number,
            String wall_type_id, String year_of_build) {
        Connection c = DbConnect.connect();
        try{
            Statement stmt = c.createStatement();
            String sql = "INSERT INTO house (address, jeu_id, level_number, "
                    + "wall_type_id, year_of_build) VALUES ('"+address+"', "
                    + jeu_id+", "+level_number+", "+wall_type_id+", '"+year_of_build+"');";
            stmt.executeUpdate(sql);
            stmt.close();
            c.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }
    
    public static void update(String id, String address, String jeu_id, 
            String level_number, String wall_type_id, String year_of_build) {
        Connection c = DbConnect.connect();
        try {
            Statement stmt = c.createStatement();
            String sql = "UPDATE house SET address = '" + address + "', "
                    + "jeu_id = " + jeu_id + ", "
                    + "level_number = " + level_number + ", "
                    + "wall_type_id = " + wall_type_id + ", "
                    + "year_of_build = '" + year_of_build + "' "
                    + "WHERE id = " + id + " ;";
            stmt.executeUpdate(sql);
            stmt.close();
            c.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }
    
    public static void delete(String id) {
        Connection c = DbConnect.connect();
        try {
            Statement stmt = c.createStatement();
            stmt.executeUpdate("DELETE FROM house WHERE id = " + id + ";");
            stmt.close();
            c.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }
    
    public static House select(String id) {
        Connection c = DbConnect.connect();
        House house = null;
        try {
            Statement stmt = c.createStatement();
            String sql = "SELECT house.id, house.address, house.jeu_id, "
                    + "house.level_number, house.wall_type_id, house.year_of_build, "
                    + "jeu.id AS jeuId, jeu.name AS jeuName, wall_type.id AS wall_typeId, "
                    + "wall_type.name AS wall_typeName "
                    + "FROM house "
                    + "INNER JOIN jeu ON house.jeu_id = jeu.id "
                    + "INNER JOIN wall_type ON house.wall_type_id = wall_type.id "
                    + "WHERE house.id = " + id + ";";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()) {
                int house_id = rs.getInt("id");
                String address = rs.getString("address");
                int jeu_id = rs.getInt("jeu_id");
                String jeu_name = rs.getString("jeuName");
                int level_number = rs.getInt("level_number");
                int wall_type_id = rs.getInt("wall_type_id");
                String wall_type_name = rs.getString("wall_typeName");
                String year_of_build = rs.getString("year_of_build");
                house = new House(house_id, address, new Jeu(jeu_id, jeu_name), 
                        level_number, new WallType(wall_type_id, wall_type_name), 
                        year_of_build);
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return house;
    }
}
