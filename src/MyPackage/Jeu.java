/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package MyPackage;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hahander
 */
public class Jeu {
    private int id;
    private String name;
    
    public Jeu(int id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    
    public static List<Jeu> getAll() {
        Connection c = DbConnect.connect();
        List<Jeu> jeus = new ArrayList<Jeu>();
        try {
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM jeu;");
            while(rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                jeus.add(new Jeu (id, name));
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        
        return jeus;
    }
    
    public static void insertJeu(String name) {
        Connection c = DbConnect.connect();
        try {
            Statement stmt = c.createStatement();
            String sql = "INSERT INTO jeu (name) VALUES ('" + name + "');";
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }
    
    public static void updateJeu(String id, String name) {
        Connection c = DbConnect.connect();
        try {
            Statement stmt = c.createStatement();
            String sql = "UPDATE jeu SET name = '" + name + "' WHERE id = " + id + ";";
            stmt.executeUpdate(sql);
            stmt.close();
            c.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }
    
    public static void deleteJeu(String id) {
        Connection c = DbConnect.connect();
        try {
            Statement stmt = c.createStatement();
            String sql = "DELETE FROM jeu WHERE id = " + id + ";";
            stmt.executeUpdate(sql);
            stmt.close();
            c.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }
}
