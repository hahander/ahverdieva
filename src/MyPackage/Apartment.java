/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package MyPackage;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hahander
 */
public class Apartment {
    private int id;
    private int number;
    private String owner;
    private Status status;
    private int rooms_count;
    private String solution_number;
    private String solution_date;
    private WorkType work_type;
    private House house;

    public Apartment(int id, int number, String owner, Status status, int rooms_count,
            String solution_number, String solution_date, WorkType work_type, 
            House house) {
        this.id = id;
        this.number = number;
        this.owner = owner;
        this.status = status;
        this.rooms_count = rooms_count;
        this.solution_number = solution_number;
        this.solution_date = solution_date;
        this.work_type = work_type;
        this.house = house;
    }
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
    
        /**
     * @return the number
     */
    public int getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(int number) {
        this.number = number;
    }

    /**
     * @return the owner
     */
    public String getOwner() {
        return owner;
    }

    /**
     * @param owner the owner to set
     */
    public void setOwner(String owner) {
        this.owner = owner;
    }

    /**
     * @return the status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * @return the rooms_count
     */
    public int getRooms_count() {
        return rooms_count;
    }

    /**
     * @param rooms_count the rooms_count to set
     */
    public void setRooms_count(int rooms_count) {
        this.rooms_count = rooms_count;
    }

    /**
     * @return the solution_number
     */
    public String getSolution_number() {
        return solution_number;
    }

    /**
     * @param solution_number the solution_number to set
     */
    public void setSolution_number(String solution_number) {
        this.solution_number = solution_number;
    }

    /**
     * @return the solution_date
     */
    public String getSolution_date() {
        return solution_date;
    }

    /**
     * @param solution_date the solution_date to set
     */
    public void setSolution_date(String solution_date) {
        this.solution_date = solution_date;
    }

    /**
     * @return the work_type
     */
    public WorkType getWork_type() {
        return work_type;
    }

    /**
     * @param work_type the work_type to set
     */
    public void setWork_type(WorkType work_type) {
        this.work_type = work_type;
    }

    /**
     * @return the house
     */
    public House getHouse() {
        return house;
    }

    /**
     * @param house the house to set
     */
    public void setHouse(House house) {
        this.house = house;
    }
    
    public static Apartment show(int apartment_id) {
        Connection c = DbConnect.connect();
        Apartment apartment = null;
        try {
            Statement stmt = c.createStatement();
            String sql = "SELECT apartment.id, apartment.number, apartment.owner, apartment.status_id, "
                    + "apartment.rooms_count, apartment.solution_number, "
                    + "apartment.solution_date, apartment.work_type_id, "
                    + "apartment.house_id, house.address, "
                    + "house.jeu_id, "
                    + "house.level_number, house.wall_type_id, house.year_of_build, "
                    + "status.name AS statusName, "
                    + "work_type.name AS workTypeName, "
                    + "jeu.name AS jeuName, wall_type.name AS wallTypeName "
                    + "FROM apartment "
                    + "INNER JOIN house ON apartment.house_id = house.id "
                    + "INNER JOIN status ON apartment.status_id = status.id "
                    + "INNER JOIN work_type ON apartment.work_type_id = work_type.id "
                    + "INNER JOIN jeu ON house.jeu_id = jeu.id "
                    + "INNER JOIN wall_type ON house.wall_type_id = wall_type.id "
                    + "WHERE apartment.id = " + apartment_id + ";";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()) {
                int id = rs.getInt("id");
                int number = rs.getInt("number");
                String owner = rs.getString("owner");
                int status_id = rs.getInt("status_id");
                String status_name = rs.getString("statusName");
                int rooms_count = rs.getInt("rooms_count");
                String solution_number = rs.getString("solution_number");
                String solution_date = rs.getString("solution_date");
                int work_type_id = rs.getInt("work_type_id");
                String work_type_name = rs.getString("workTypeName");
                int house_id = rs.getInt("house_id");
                String address = rs.getString("address");
                int jeu_id = rs.getInt("jeu_id");
                String jeu_name = rs.getString("jeuName");
                int level_number = rs.getInt("level_number");
                int wall_type_id = rs.getInt("wall_type_id");
                String wall_type_name = rs.getString("wallTypeName");
                String year_of_build = rs.getString("year_of_build");
                apartment = new Apartment(id, number, owner, new Status(status_id, status_name), 
                        rooms_count, solution_number, solution_date, 
                        new WorkType(work_type_id, work_type_name), 
                        new House(house_id, address, new Jeu(jeu_id, jeu_name), 
                                level_number, new WallType(wall_type_id, 
                                        wall_type_name), year_of_build));
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return apartment;
    }
    
    public static List<Apartment> select(String houseid) {
        List<Apartment> apartments = new ArrayList<Apartment>();
        Connection c = DbConnect.connect();
        try {
            Statement stmt = c.createStatement();
            String sql = "SELECT apartment.id, apartment.number, apartment.owner, apartment.status_id, "
                    + "apartment.rooms_count, apartment.solution_number, "
                    + "apartment.solution_date, apartment.work_type_id, "
                    + "apartment.house_id, house.address, "
                    + "house.jeu_id, "
                    + "house.level_number, house.wall_type_id, house.year_of_build, "
                    + "status.name AS statusName, "
                    + "work_type.name AS workTypeName, "
                    + "jeu.name AS jeuName, wall_type.name AS wallTypeName "
                    + "FROM apartment "
                    + "INNER JOIN house ON apartment.house_id = house.id "
                    + "INNER JOIN status ON apartment.status_id = status.id "
                    + "INNER JOIN work_type ON apartment.work_type_id = work_type.id "
                    + "INNER JOIN jeu ON house.jeu_id = jeu.id "
                    + "INNER JOIN wall_type ON house.wall_type_id = wall_type.id "
                    + "WHERE apartment.house_id = " + houseid + ";";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()) {
                int id = rs.getInt("id");
                int number = rs.getInt("number");
                String owner = rs.getString("owner");
                int status_id = rs.getInt("status_id");
                String status_name = rs.getString("statusName");
                int rooms_count = rs.getInt("rooms_count");
                String solution_number = rs.getString("solution_number");
                String solution_date = rs.getString("solution_date");
                int work_type_id = rs.getInt("work_type_id");
                String work_type_name = rs.getString("workTypeName");
                int house_id = rs.getInt("house_id");
                String address = rs.getString("address");
                int jeu_id = rs.getInt("jeu_id");
                String jeu_name = rs.getString("jeuName");
                int level_number = rs.getInt("level_number");
                int wall_type_id = rs.getInt("wall_type_id");
                String wall_type_name = rs.getString("wallTypeName");
                String year_of_build = rs.getString("year_of_build");
                apartments.add(new Apartment(id, number, owner, 
                        new Status(status_id, status_name), rooms_count, 
                        solution_number, solution_date, 
                        new WorkType(work_type_id, work_type_name), 
                        new House(house_id, address, 
                                new Jeu(jeu_id, jeu_name), level_number, 
                                new WallType(wall_type_id, wall_type_name), 
                                year_of_build)));
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return apartments;
    }

    public static void insert(String number, String owner, String status_id, String rooms_count,
            String solution_number, String solution_date, String work_type_id,
            String house_id) {
        Connection c = DbConnect.connect();
        try {
            Statement stmt = c.createStatement();
            String sql = "INSERT INTO apartment (number, owner, status_id, rooms_count, "
                    + "solution_number, solution_date, work_type_id, house_id) "
                    + "VALUES ("+number+", '"+owner+"', "+status_id+", "+rooms_count+", "
                    + "'"+solution_number+"', '"+solution_date+"', "+work_type_id+", "
                    + ""+house_id+");";
            stmt.executeUpdate(sql);
            stmt.close();
            c.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }
    
    public static void update(String id, String number, String owner, String status_id, 
            String rooms_count, String solution_number, String solution_date, 
            String work_type_id) {
        Connection c = DbConnect.connect();
        try {
            Statement stmt = c.createStatement();
            String sql = "UPDATE apartment SET number = "+number+", owner = '" + owner + "',"
                    + "status_id = " +status_id + ", rooms_count = "
                    + rooms_count +", solution_date = '"+solution_date+"', "
                    + "solution_number = "+solution_number+", "
                    + "work_type_id = "+work_type_id+" WHERE id = "+id+";";
            stmt.executeUpdate(sql);
            stmt.close();
            c.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }
    
    public static void delete(int id) {
        Connection c = DbConnect.connect();
        try {
            Statement stmt = c.createStatement();
            stmt.executeUpdate("DELETE FROM apartment WHERE id = "+id+";");
            stmt.close();
            c.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }


}
