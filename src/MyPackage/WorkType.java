/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package MyPackage;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hahander
 */
public class WorkType {
    private int id;
    private String name;
    
    public WorkType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    
    public static List<WorkType> selectAll() {
        Connection c = DbConnect.connect();
        List<WorkType> work_types = new ArrayList<WorkType>();
        try {
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM work_type;");
            while(rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                work_types.add(new WorkType(id, name));
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return work_types;
    }
    
    public static void insert(String name) {
        Connection c = DbConnect.connect();
        try {
            Statement stmt = c.createStatement();
            String sql = "INSERT INTO work_type (name) VALUES ('" +name + "');";
            stmt.executeUpdate(sql);
            stmt.close();
            c.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }
    
    public static void update(String id, String name) {
        Connection c = DbConnect.connect();
        try {
            Statement stmt = c.createStatement();
            String sql = "UPDATE work_type SET name = '" + name 
                    + "' WHERE id = " + id + ";";
            stmt.executeUpdate(sql);
            stmt.close();
            c.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }
    
    public static void delete(String id) {
        Connection c = DbConnect.connect();
        try {
            Statement stmt = c.createStatement();
            stmt.executeUpdate("DELETE FROM work_type WHERE id = " + id + ";");
            stmt.close();
            c.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    } 
}
