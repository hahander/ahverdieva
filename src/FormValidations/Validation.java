/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package FormValidations;

import MyPackage.*;
import java.awt.Component;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.ToolTipManager;

/**
 *
 * @author hahander
 */


public class Validation {
    public static Boolean notEmpty(JFrame frame) {
        Boolean check = true;
        for (Component C : frame.getContentPane().getComponents()) {
            if (C instanceof JTextField) {
                if (((JTextField)C).getText().isEmpty()) {
                    check = false;
                }
            }
        }
        return check;
    }
}
