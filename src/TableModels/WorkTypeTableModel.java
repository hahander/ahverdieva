/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TableModels;

import MyPackage.WorkType;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author hahander
 */
public class WorkTypeTableModel extends AbstractTableModel{
    private List<WorkType> li = new ArrayList();
    private String[] columnNames = {"Код", "Наименование"};
    
    public WorkTypeTableModel(List<WorkType> list) {
        this.li = list;
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }
    
    @Override
    public int getRowCount() {
        return li.size();
    }
    
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        WorkType work_type = li.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return work_type.getId();
            case 1:
                return work_type.getName();
        }
        return null;
    }
    
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0:
                return Integer.class;
            case 1:
                return String.class;
        }
        return null;
    }
}
