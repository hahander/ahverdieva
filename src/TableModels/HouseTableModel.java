/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TableModels;

import MyPackage.House;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author hahander
 */
public class HouseTableModel extends AbstractTableModel {
    private List<House> li = new ArrayList();
    private String[] columnNames = {"Код", "Адрес", "Жэу", "Этажность", 
        "Тип стен", "Год постройки"};
    
    public HouseTableModel(List<House> list) {
        this.li = list;
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }
    
    @Override
    public int getRowCount() {
        return li.size();
    }
    
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        House house = li.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return house.getId();
            case 1:
                return house.getAddress();
            case 2:
                return house.getJeu().getName();
            case 3:
                return house.getLevel_number();
            case 4:
                return house.getWall_type().getName();
            case 5:
                return house.getYear_of_build();
        }
        return null;
    }
    
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0:
                return Integer.class;
            case 1:
                return String.class;
            case 2:
                return String.class;
            case 3: 
                return Integer.class;
            case 4:
                return String.class;
            case 5:
                return String.class;
        }
        return null;
    }
}
