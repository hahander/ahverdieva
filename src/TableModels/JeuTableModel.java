/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TableModels;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import MyPackage.Jeu;

/**
 *
 * @author hahander
 */
public class JeuTableModel extends AbstractTableModel {
    private List<Jeu> li = new ArrayList();
    private String[] columnNames = {"Код", "Наименование"};
    
    public JeuTableModel(List<Jeu> list) {
        this.li = list;
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }
    
    @Override
    public int getRowCount() {
        return li.size();
    }
    
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Jeu jeu = li.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return jeu.getId();
            case 1:
                return jeu.getName();
        }
        return null;
    }
    
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0:
                return Integer.class;
            case 1:
                return String.class;
        }
        return null;
    }
}
