/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TableModels;

import MyPackage.WallType;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author hahander
 */
public class WallTypeTableModel extends AbstractTableModel {
    private List<WallType> li = new ArrayList();
    private String[] columnNames = {"Код", "Наименование"};
    
    public WallTypeTableModel(List<WallType> list) {
        this.li = list;
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }
    
    @Override
    public int getRowCount() {
        return li.size();
    }
    
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        WallType wall_type = li.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return wall_type.getId();
            case 1:
                return wall_type.getName();
        }
        return null;
    }
    
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0:
                return Integer.class;
            case 1:
                return String.class;
        }
        return null;
    }
}
