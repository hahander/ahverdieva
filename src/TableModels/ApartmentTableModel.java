/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TableModels;

import MyPackage.Apartment;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author hahander
 */
public class ApartmentTableModel extends AbstractTableModel {
    private List<Apartment> li = new ArrayList();
    private String[] columnNames = {"Код", "Номер", "Владелец", "Статус", "Кол-во комнат", 
        "Номер решения", "Дата решения", "Тип работ", "Адрес"};
    
    public ApartmentTableModel(List<Apartment> list) {
        this.li = list;
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }
    
    @Override
    public int getRowCount() {
        return li.size();
    }
    
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Apartment apartment = li.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return apartment.getId();
            case 1:
                return apartment.getNumber();
            case 2:
                return apartment.getOwner();
            case 3:
                return apartment.getStatus().getName();
            case 4:
                return apartment.getRooms_count();
            case 5:
                return apartment.getSolution_number();
            case 6:
                return apartment.getSolution_date();
            case 7:
                return apartment.getWork_type().getName();
            case 8:
                return apartment.getHouse().getAddress();
        }
        return null;
    }
    
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0:
                return Integer.class;
            case 1:
                return Integer.class;
            case 2:
                return String.class;
            case 3:
                return String.class;
            case 4: 
                return Integer.class;
            case 5:
                return String.class;
            case 6:
                return String.class;
            case 7:
                return String.class;
            case 8:
                return String.class;    
        }
        return null;
    }
}
